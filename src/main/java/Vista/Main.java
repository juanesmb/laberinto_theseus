package Vista;

import Modelo.Laberinto;

/**
 *
 * @author 1151941 - 1151858
 */
public class Main {

    public static void main(String[] args) {
//        String url = "http://madarme.co/persistencia/laberintoTeseo.txt"; //Prueba 1 
//        String url = "http://madarme.co/persistencia/laberintoTeseo2.txt"; //Prueba 3
//        String url = "http://madarme.co/persistencia/laberintoTeseo3.txt"; //Prueba 2
        String url = "https://gitlab.com/-/snippets/2033398/raw/master/laberinto.txt"; // Prueba 4

        try {
            Laberinto l = new Laberinto(url);
            System.out.println(l.toString());
            System.out.println("El camino recorrido es:" + l.getCamino());
            System.out.println("\n----------------Laberinto recorrido-------------------------");
            System.out.print(l.toString());

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
