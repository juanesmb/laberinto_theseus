/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author juan moncada
 */
public class Punto 
{
    private short fila;
    private short columna;
    
    Punto(short fila, short columna)
    {
        this.fila = fila;
        this.columna = columna;
    }

    public short getFila() {
        return fila;
    }

    public short getColumna() {
        return columna;
    }

    public void setFila(short fila) {
        this.fila = fila;
    }

    public void setColumna(short columna) {
        this.columna = columna;
    }

    @Override
    public String toString() 
    {
        return '(' + fila + "," + columna + ')';
    }
}
