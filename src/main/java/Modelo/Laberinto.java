package Modelo;

import Util.ArchivoLeerURL;
import Util.Cola;

/**
 *
 * @author 1151941 - 1151858
 */
public class Laberinto {

    private short[][] laberinto;
    private Punto teseo;
    private final Cola<String> caminos = new Cola();

    public Laberinto(String url) {
        this.leerUrl(url);
    }

    private void leerUrl(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        this.laberinto = new short[v.length][];

        for (int i = 0; i < v.length; i++) {
            String linea = v[i].toString();
            String[] datos = linea.split(";");
            this.laberinto[i] = new short[datos.length];

            for (int j = 0; j < datos.length; j++) {
                short n = Short.parseShort(datos[j]);

                if (n != 0 && n != 1 && n != -1 && n != 1000 && n != 100 && n != 9) {
                    throw new RuntimeException("Hay valores de la matriz inválidos");
                }

                this.laberinto[i][j] = n;

                if (n == 9) {
                    this.teseo = new Punto((short) i, (short) j);
                }
            }
        }
    }

    public String getCamino() {
        String camino = "(" + this.teseo.getFila() + "," + this.teseo.getColumna() + ")";
        this.getCamino(this.teseo.getFila(), this.teseo.getColumna(), camino, true);

        if (this.caminos.esVacia()) {
            throw new RuntimeException("No hay salida");
        }

        String menor = null;

        for (int i = 0; i < this.caminos.getTamanio(); i++) {
            String c = this.caminos.deColar();

            if (menor == null || c.length() < menor.length()) {
                menor = c;
            }
        }

        String[] split = menor.split("-");

        for (String s : split) {
            short y = Short.parseShort(String.valueOf(s.charAt(1)));
            short x = Short.parseShort(String.valueOf(s.charAt(3)));

            if (this.laberinto[y][x] == 0) {
                this.laberinto[y][x] = 1;
            }

            if (this.laberinto[y][x] == 100) {
                this.laberinto[y][x] = -100;
            }
        }

        return menor;
    }

    public void getCamino(short fTeseo, short cTeseo, String camino, boolean vivo) {
        if (this.laberinto[fTeseo][cTeseo] == 1000 && !vivo) {
            caminos.enColar(camino);
        }

        if (this.laberinto[fTeseo][cTeseo] == 0) {
            this.laberinto[fTeseo][cTeseo] = 1;
        }

        if (this.laberinto[fTeseo][cTeseo] == 100) {
            vivo = false;
            this.laberinto[fTeseo][cTeseo] = -100;
        }

        if (puedeSubir(fTeseo, cTeseo, vivo)) {
            this.mover((short) (fTeseo - 1), cTeseo, camino, vivo);
        }

        if (puedeBajar(fTeseo, cTeseo, vivo)) {
            this.mover((short) (fTeseo + 1), cTeseo, camino, vivo);
        }

        if (puedeCruzarALaDerecha(fTeseo, cTeseo, vivo)) {
            this.mover(fTeseo, (short) (cTeseo + 1), camino, vivo);
        }

        if (puedeCruzarALaIzquierda(fTeseo, cTeseo, vivo)) {
            this.mover(fTeseo, (short) (cTeseo - 1), camino, vivo);
        }
    }

    private void mover(short fila, short columna, String camino, boolean vivo) {
        camino += "-(" + fila + "," + columna + ")";

        this.getCamino(fila, columna, camino, vivo);

        if (this.laberinto[fila][columna] == 1) {
            this.laberinto[fila][columna] = 0;
        }

        if (this.laberinto[fila][columna] == -100) {
            this.laberinto[fila][columna] = 100;
        }
    }

    private boolean puedeSubir(short fTeseo, short cTeseo, boolean vivo) {
        return esCamino((short) (fTeseo - 1), cTeseo, vivo);
    }

    private boolean puedeBajar(short fTeseo, short cTeseo, boolean vivo) {
        return esCamino((short) (fTeseo + 1), cTeseo, vivo);
    }

    private boolean puedeCruzarALaDerecha(short fTeseo, short cTeseo, boolean vivo) {
        return esCamino(fTeseo, (short) (cTeseo + 1), vivo);
    }

    private boolean puedeCruzarALaIzquierda(short fTeseo, short cTeseo, boolean vivo) {
        return esCamino(fTeseo, (short) (cTeseo - 1), vivo);
    }

    private boolean esCamino(short fila, short col, boolean vivo) {
        if (fila < 0 || fila >= this.laberinto.length) {
            return false;
        }

        if (col < 0 || col >= this.laberinto[fila].length) {
            return false;
        }

        short valor = this.laberinto[fila][col];

        return valor == 0 || valor == 100 || valor == 1000 && !vivo;
    }

    @Override
    public String toString() {
        String msg = "";

        for (short[] l : this.laberinto) {
            for (int j = 0; j < l.length; j++) {
                msg += l[j] + "\t";
            }
            msg += "\n";
        }
        return msg;
    }
}
